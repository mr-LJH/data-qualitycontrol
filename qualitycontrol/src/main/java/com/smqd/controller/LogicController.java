package com.smqd.controller;

import com.smqd.entity.*;
import com.smqd.service.LogicService;
import com.smqd.utils.BloomFilterUtil;
import com.smqd.utils.JDBCUtil;
import com.smqd.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;

@RestController
@RequestMapping(value = "/logic")
public class LogicController {
    @Autowired
    private LogicService logicService;

    @Autowired
    RedisUtils redisUtils;
    @Autowired
    BloomFilterUtil bloomFilterUtil;

    /***
     * 查询方法
     * @return
     */
    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public void findAll() {

        try {
            List<DataBaseMessage> data = logicService.selectAll();

            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String format = formatter.format(date);
            for (DataBaseMessage datum : data) {
                //查询musql中的 医院数据库中表数量
                List<QuanlityTable> gpTables = logicService.selectQuanlityTable();
                //查询医院数据库中表数量
                String sourceQuality = qualityControl(datum);
                //插入方法
                if (gpTables.size() == 0) {
                    QuanlityTable table = new QuanlityTable();
                    table.setSourceTableName(sourceQuality);
                    table.setCreateTime(format);
                    table.setSourceUrl(datum.getSourceUrl());
                    logicService.insertQuanlityTables(table);
                } else {
                    QuanlityTable quanlityTable = logicService.selectQuanlityTableDesc(datum.getSourceUrl());
                    if (!ObjectUtils.isEmpty(quanlityTable)) {
                        if (quanlityTable.getSourceUrl().equals(datum.getSourceUrl())) {
                            String gptableName = quanlityTable.getSourceTableName();
                            String[] sourceSplit = sourceQuality.split(",");
                            String[] gpSplit = gptableName.split(",");
                            StringBuffer buffer = new StringBuffer();
                            TreeSet<String> set = new TreeSet<>();
                            for (String s1 : gpSplit) {
                                set.add(s1);
                            }
                            for (String s : sourceSplit) {
                                boolean add = set.add(s);
                                if (add) {
                                    buffer.append(s).append(",");
                                }

                            }
                            String substring = null;
                            if (buffer.length() != 0) {
                                int i = buffer.toString().lastIndexOf(",");
                                substring = buffer.toString().substring(0, i);
                            }
                            if (!ObjectUtils.isEmpty(substring)) {
                                QuanlityTable table = new QuanlityTable();
                                table.setNewTableName(substring);
                                table.setSourceTableName(sourceQuality);
                                table.setSourceUrl(datum.getSourceUrl());
                                table.setCreateTime(format);
                                logicService.insertQuanlityTables(table);
                            }
                        }
                    } else {
                        QuanlityTable table = new QuanlityTable();
                        table.setSourceTableName(sourceQuality);
                        table.setCreateTime(format);
                        table.setSourceUrl(datum.getSourceUrl());
                        logicService.insertQuanlityTables(table);
                    }
                }

            }


            for (DataBaseMessage datum : data) {
                //质控
                QualityResult qualityResult;
                if (datum.getFindFalg() == 0) {


                    //查询医院数据
                    //创建连接
                    Connection connection = JDBCUtil.getConnection(datum.getSourceUserName(), datum.getSourcePassWord(), datum.getSourceUrl(), datum.getSourceType());
                    //查询源库返回条数
                    int i = executeSourceNum(connection, datum.getSourceType(), datum.getSourceTableName());

                    //创建连接
                    Connection connect = JDBCUtil.getConnection(datum.getTargetUserName(), datum.getTargetpassWord(), datum.getTargetUrl(), "mysql");
                    //查询目标库返回的条数
                    int i2 = executeTargetNum(connect, datum.getTargetTableName());
                    qualityResult = new QualityResult();
                    qualityResult.setCreateTime(format);
                    qualityResult.setSourceUrl(datum.getSourceUrl());
                    qualityResult.setSourceNum(i);
                    qualityResult.setSourceTableName(datum.getSourceTableName());
                    qualityResult.setTargeTableName(datum.getTargetTableName());
                    qualityResult.setTargeNum(i2);
                    //将查询出来的数据添加入库
                    logicService.insertQualityResult(qualityResult);
                } else {
                    //获取医院数据库连接
                    Connection connection = JDBCUtil.getConnection(datum.getSourceUserName(), datum.getSourcePassWord(), datum.getSourceUrl(), datum.getSourceType());
                    //查询医院数据库中的主键，
                    List<String> list = executeSource(connection, datum.getSourceType(), datum.getSourceTableName(), datum.getPrimaryKey());
                    //生命过滤器
                    bloomFilterUtil.getStringBloomFilter("");
                    //将主键存放到过滤器中
                    for (String s : list) {
                        bloomFilterUtil.add(s);
                    }
                    //获取医院端数据库连接
                    Connection postgresql = JDBCUtil.getConnection(datum.getTargetUserName(), datum.getTargetpassWord(), datum.getTargetUrl(), "postgresql");
                    //查询医院数据库中的主键
                    List<String> list1 = executeTarget(postgresql, datum.getTargetTableName(), datum.getPrimaryKey());
                    StringBuffer stringBuffer = new StringBuffer();
                    for (String s : list1) {
                        Boolean check = bloomFilterUtil.check(s);
                        if (!check) {
                            stringBuffer.append(s).append(",");
                        }
                    }
                    qualityResult = new QualityResult();

                    qualityResult.setCreateTime(format);
                    qualityResult.setSourceUrl(datum.getSourceUrl());
                    qualityResult.setSourceTableName(datum.getSourceTableName());
                    qualityResult.setTargeTableName(datum.getTargetTableName());
                    qualityResult.setEscapeField(stringBuffer.toString());
                    logicService.insertQualityResult(qualityResult);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     * 查询质控结果
     * @return
     */
    @RequestMapping(value = "/findQuanlitiResultAll", method = RequestMethod.GET)
    public List<QualityResult> findQuanlitiResultAll() {
        List<QualityResult> qualityResults = logicService.findQuanlitiResultAll();
        return qualityResults;
    }

    /**
     * @param
     * @return java.lang.String
     * 这是查询医院数据库的表名
     * @author yjj
     * @date 2021/8/4 14:22
     */
    public String qualityControl(DataBaseMessage baseMessage) {
        StringBuffer buffer = new StringBuffer();

        //源库的库名
        String sourceType = baseMessage.getSourceType();
        if (sourceType.equals("mysql")) {
            String mysqlTableName = findMysqlTableName(baseMessage.getSourceUserName(), baseMessage.getSourcePassWord(), baseMessage.getSourceUrl()
                    , baseMessage.getSourcedbName());

            buffer.append(mysqlTableName);

        } else if (sourceType.equals("oracle")) {

            String oracleTableName = findOracleTableName(baseMessage.getSourceUserName(), baseMessage.getSourcePassWord(), baseMessage.getSourceUrl()
                    , null);
            buffer.append(oracleTableName);
        } else if (sourceType.equals("sqlserver")) {
            String sqlserverTableName = findSqlServerTableName(baseMessage.getSourceUserName(), baseMessage.getSourcePassWord(), baseMessage.getSourceUrl()
                    , null);
            buffer.append(sqlserverTableName);
        }
        String s = null;
        if (buffer.length() != 0 && buffer.toString().contains(",")) {
            int i = buffer.toString().lastIndexOf(",");
            s = buffer.substring(0, i).toString();
        }


        return s;
    }


    /***
     * 查询mysql的表名
     * @param userName
     * @param password
     * @param url
     * @param dataBaseName
     * @return
     */
    public String findMysqlTableName(String userName, String password, String url, String dataBaseName) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        StringBuffer buffer = new StringBuffer();

        try {
            String sql = "SELECT  table_name FROM information_schema.TABLES  WHERE table_schema = " + "'" + dataBaseName + "'";
            connection = JDBCUtil.getConnection(userName, password, url, "mysql");
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                String string = resultSet.getString(1);
                buffer.append(string).append(",");
                System.out.println(string);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtil.release(resultSet, statement, connection);
        }
        return buffer.toString();

    }

    /***
     * 查询oracle的表名
     * @param userName
     * @param password
     * @param url
     * @param dataBaseName
     * @return
     */
    public String findOracleTableName(String userName, String password, String url, String dataBaseName) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        StringBuffer buffer = new StringBuffer();
        try {
            String sql = "select TABLE_name from tabs";
            connection = JDBCUtil.getConnection(userName, password, url, "oracle");
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                String string = resultSet.getString(1);
                buffer.append(string).append(",");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtil.release(resultSet, statement, connection);
        }
        return buffer.toString();
    }

    /***
     * 查询sqlserver的表名
     * @param userName
     * @param password
     * @param usr
     * @param dataBaseName
     * @return
     */
    public String findSqlServerTableName(String userName, String password, String usr, String dataBaseName) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        StringBuffer buffer = new StringBuffer();
        try {
            String sql = "select name from sysobjects where xtype='u'";
            //创建连接
            connection = JDBCUtil.getConnection(userName, password, usr, "sqlserver");
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                String string = resultSet.getString(1);
                buffer.append(string).append(",");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtil.release(resultSet, statement, connection);
        }
        return buffer.toString();

    }

    /***
     * 查询gp数据库中主键
     * @param conn
     * @param tableNname
     * @param primaryKey
     * @return
     */
    public List<String> executeTarget(Connection conn, String tableNname, String primaryKey) {
        Statement statement = null;
        ResultSet resultSet = null;
        List<String> list = null;
        try {
            list = new ArrayList<>();
            String date = getDate();
            String sql = "SELECT \"" + primaryKey + " \" FROM  " + tableNname + "where \"increment_time\">" + date + "  ";
            statement = conn.createStatement();
            resultSet = statement.executeQuery(sql);
            String[] pk = primaryKey.split(",");
            StringBuffer buffer = new StringBuffer();
            while (resultSet.next()) {
                for (int i = 0; i < pk.length; i++) {
                    String string = resultSet.getString(pk[i]);
                    buffer.append(string).append("@@##$$");
                }
                list.add(buffer.toString());
                buffer.delete(0, buffer.length());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtil.release(resultSet, statement, conn);
        }
        return list;
    }

    /***
     * 查询源库的主键
     * @param conn
     * @param type
     * @param tableNname
     * @return
     */
    public List<String> executeSource(Connection conn, String type, String tableNname, String primaryKey) {
        String sql = null;

        Statement statement = null;
        ResultSet resultSet = null;
        List<String> list = null;
        try {
            String[] split = tableNname.split(",");
            list = new ArrayList<>();
            for (String s : split) {
                if ("mysql".equals(type)) {
                    sql = "select " + primaryKey + "  from " + s + "";
                }
                if ("oracle".equals(type)) {
                    sql = "select  \"" + primaryKey + "\" from  " + "\"" + s + "\" ";
                }
                if ("sqlserver".equals(type)) {
                    sql = "SELECT " + primaryKey + "  FROM  " + s + " ";
                }

                statement = conn.createStatement();
                resultSet = statement.executeQuery(sql);
                String[] pk = primaryKey.split(",");
                StringBuffer buffer = new StringBuffer();
                while (resultSet.next()) {
                    for (int i = 0; i < pk.length; i++) {
                        String s1 = pk[i].replaceAll("\"", "");
                        System.out.println(s1);
                        String string = resultSet.getString(s1);
                        buffer.append(string).append("@@##$$");
                    }

                }
                list.add(buffer.toString());
                buffer.delete(0, buffer.length());
            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtil.release(resultSet, statement, conn);
        }
        return list;
    }


    /***
     * 查询源库的条数
     * @param conn
     * @param type
     * @param tableNname
     * @return
     */

    public int executeSourceNum(Connection conn, String type, String tableNname) {
        String sql = null;
        int anInt = 0;
        Statement statement = null;
        ResultSet resultSet = null;
        Num num = new Num();
        int a = 1;
        try {

            String[] split = tableNname.split(",");
            for (String s : split) {
                if ("mysql".equals(type)) {
                    sql = "select count(*) as number from " + s + "";
                }
                if ("oracle".equals(type)) {
                    sql = "select count(*) as \" number  \" from " + "\"" + s + "\" ";
                }
                if ("sqlserver".equals(type)) {
                    sql = "SELECT COUNT(*) as number FROM  " + s + " ";
                }

                statement = conn.createStatement();
                resultSet = statement.executeQuery(sql);

                while (resultSet.next()) {
                    anInt = resultSet.getInt(1);
                }

                if (a == split.length) {
                    num.setNum2(anInt);
                } else {
                    num.setNum1(anInt);
                }
                a++;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtil.release(resultSet, statement, conn);
        }
        return num.getNum1() + num.getNum2();
    }

    class Num {
        int num1;
        int num2;

        public int getNum1() {
            return num1;
        }

        public void setNum1(int num1) {
            this.num1 = num1;
        }

        public int getNum2() {
            return num2;
        }

        public void setNum2(int num2) {
            this.num2 = num2;
        }
    }

    /***
     * 查询目标库的条数
     * @param conn
     * @param tableNname
     * @return
     */
    public int executeTargetNum(Connection conn, String tableNname) {

        int anInt = 0;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            String sql = "SELECT count(*) FROM " + "\"" + tableNname + "\" ";
//            String sql = "SELECT count(*) FROM " + "" + tableNname + " ";
            statement = conn.createStatement();
            resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                anInt = resultSet.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtil.release(resultSet, statement, conn);
        }
        return anInt;
    }

    /***
     * 获取当前时间
     * @return
     */
    public String getDate() {
        String sd2 = null;
        try {

            Date date = new Date();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String today = simpleDateFormat.format(date.getTime());
            System.out.println("当天日期" + today);

            //1、当天凌晨(毫秒)
            long daytime1 = simpleDateFormat.parse(today).getTime();
            System.out.println("1、当天凌晨(毫秒)" + daytime1);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sd2 = sdf.format(new Date(Long.parseLong(String.valueOf(daytime1))));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sd2;

    }

    public static void main(String[] args) {
//        Date date = new Date();
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        System.out.println(formatter.format(date));
        TreeSet<String> strings = new TreeSet<>();
        boolean add = strings.add("1");
        System.out.println(add);
        boolean add11 = strings.add("1");
        System.out.println(add11);
    }

}
