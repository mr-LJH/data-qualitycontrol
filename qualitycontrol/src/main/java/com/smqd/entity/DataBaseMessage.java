package com.smqd.entity;

import lombok.Data;

/**
 * @author yjj
 * @date 2021年08月02日18:25
 */
@Data
public class DataBaseMessage {
    private Integer id;
    private String sourceUserName;
    private String sourceUrl;
    private String sourcePassWord;
    private String sourceTableName;
    private String targetUserName;
    private String targetpassWord;
    private String targetUrl;
    private String targetTableName;
    private String sourceType;
    private int findFalg;
    private String primaryKey;
    private String sourcedbName;
}
