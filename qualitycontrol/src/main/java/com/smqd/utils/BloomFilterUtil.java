package com.smqd.utils;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RBloomFilter;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;

@Slf4j
@Service
public class BloomFilterUtil {

    private static int size = 10000000;

    private static BloomFilter<String> bloomFilter = null;




    /****
     * 创建布隆过滤器对象
     * @return
     */
    public  BloomFilter<String> getStringBloomFilter(String path) {
        try {
            File file = new File(path);
            if (file.exists()) {
                FileInputStream fileInputStream = new FileInputStream(file);
                bloomFilter = BloomFilter.readFrom(fileInputStream, Funnels.stringFunnel(Charset.defaultCharset()));
            } else {
                bloomFilter = BloomFilter.create(Funnels.stringFunnel(Charset.defaultCharset()), size, 0.0001);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return bloomFilter;
    }

    /***
     * 添加数据
     * @param data
     * @return
     */
    public  Boolean add(String data) {
        boolean put = bloomFilter.put(data);
        if (!put) {
            log.info("数据存放失败" + data);
        }
        return put;
    }

    /***
     * 检查数据是否存在
     * @param data
     * @return
     */
    public  Boolean check(String data) {
        boolean b = bloomFilter.mightContain(data);

        if (!b) {
            log.info("过滤器中不存在   " + data + "   ");
        }
        return b;
    }


    /***
     * 将过滤器中的数值保存到磁盘
     * @param fileName
     */
    public  void write(String fileName) {
        try {
            bloomFilter.writeTo(new FileOutputStream(new File(fileName)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


//    public static void main(String[] args) {
//        Config config = new Config();
//        config.useSingleServer().setAddress("redis://192.168.0.6:6379");
//        RedissonClient redisson = Redisson.create(config);
//        RBloomFilter<Object> list = redisson.getBloomFilter("phoneList");
//        list.tryInit(3, 0.03);
//        list.add("10086");
//        long count = list.count();
//        if (!list.contains("123456")) {
//            list.delete();
//
//        }
//
//
//        redisson.shutdown();
//    }


    //获取一个布隆过滤器
    //fileName:持久化的文件名,这里是相对路径,就在本项目下
//    public static BloomFilter<String> getStringBloomFilter(String fileName) {
//
//        try {
//            File file = new File("filter");
//            if (!file.exists()) {
//                System.out.println("持久化文件不存在!,将创建文件,布隆过滤器为空");
//                bloomFilter = BloomFilter.create(Funnels.stringFunnel(Charset.defaultCharset()), size);
//            } else {
//                System.out.println("持久化文件存在!,从文件读取数据到布隆过滤器");
//                FileInputStream fileInputStream = new FileInputStream(file);
//
//                bloomFilter = BloomFilter.readFrom(fileInputStream, Funnels.stringFunnel(Charset.defaultCharset()));
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return bloomFilter;
//    }


}
