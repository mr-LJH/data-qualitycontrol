package com.smqd.entity;

import lombok.Data;

/**
 * @author yjj
 * @date 2021年08月04日14:11
 */
@Data
public class QuanlityTable {
    private Integer id;
    private String sourceTableName;
    private String newTableName;
    private String createTime;
}
