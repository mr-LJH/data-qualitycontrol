package com.smqd.entity;

import lombok.Data;

/**
 * @author yjj
 * @date 2021年08月03日9:28
 */
@Data
public class QualityResult {
    private Integer id;
    private String sourceUrl;
    private String sourceTableName;
    private Integer sourceNum;
    private Integer targeNum;
    private String createTime;
    private String targeTableName;
    private String escapeField;
}
