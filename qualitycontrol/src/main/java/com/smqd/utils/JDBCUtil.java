package com.smqd.utils;

import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.sql.*;
import java.util.*;


@Slf4j
public class JDBCUtil {
    //准备数据库的四大参数：
    // private static final String driver = "com.mysql.jdbc.Driver";
//    private static final String url = "jdbc:mysql://localhost:3306/copy";
//    private static final String username = "root";
//    private static final String password = "123456";


    /***
     *创建工具类： 直接获得一个连接对象：
     * @return
     */
    public static Connection getConnection(String username, String password, String url, String type) {
        String driver = null;
        if ("mysql".equals(type)) {
            driver = "com.mysql.jdbc.Driver";
        }
        if ("oracle".equals(type)) {
            driver = "oracle.jdbc.driver.OracleDriver";
        }
        if ("sqlserver".equals(type)) {
            driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        }
        if ("postgresql".equals(type)) {
            driver = "org.postgresql.Driver";
        }
        try {
            Class.forName(driver);
            Connection connection = DriverManager.getConnection(url, username, password);
            return connection;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

//    public static Connection testConnection(String username, String password, String url, String driver) {
//        try {
//            Class.forName(driver);
//            Connection connection = DriverManager.getConnection(url, username, password);
//            return connection;
//
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//    }

    /***
     * 定义一个方法： 释放资源： 直接将rs stmt conn 全部释放：
     */

    public static void release(ResultSet rs, Statement stmt, Connection conn) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            rs = null;
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            stmt = null;
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            conn = null;
        }

    }

//    public static void main(String[] args) throws Exception {
////        final String driver = "org.postgresql.Driver";
////        final String url = "jdbc:postgresql://10.0.6.112:5432/postgres";
////        final String username = "gpadmin";
////        final String password = "gpadmin";
////        String sql = " SELECT * FROM postgres.\"public\".testbean";
//        final String driver = "com.mysql.jdbc.Driver";
//        final String url = "jdbc:mysql://192.168.0.30/genncdm";
//        final String username = "root";
//        final String password = "123456";
//        String sql = " SELECT id as id1 , taskName from  sqltask ";
//        Connection connection = JDBCUtil.getConnection(username, password, url, driver);
//        Statement statement = connection.createStatement();
//        ResultSet resultSet = statement.executeQuery(sql);
//        Map<String, Object> map = JDBCUtil.returnData(resultSet);
//        String records = map.get("records").toString();
//        HashMap<String, String> map1 = new HashMap<>();
//        String s = records.replaceAll("=", ":");
//        //转为json对象
//        JSONObject jsonObject = JSONObject.parseObject(s);
//        //遍历json对象获取key  value
//        Iterator iter = jsonObject.entrySet().iterator();
//        while (iter.hasNext()) {
//            Map.Entry entry = (Map.Entry) iter.next();
//            //把结果村存放到map中，供后续替换用
//            map1.put(entry.getKey().toString(), entry.getValue().toString());
//        }
//
//        System.out.println(map1.toString());
//
//    }


    /***
     * 将结果集转json,并返回表中的字段
     * @param rs
     * @return
     */
    public static Map<String, Object> returnData(ResultSet rs) {
        List records = new ArrayList();
        Map<String, Object> map = new HashMap<>();
        try {

            Set<Object> set = new TreeSet<>();
            ResultSetMetaData rsmd = rs.getMetaData();
            int fieldCount = rsmd.getColumnCount();

            while (rs.next()) {
                Map<String, String> valueMap = new LinkedHashMap<String, String>();
                for (int i = 1; i <= fieldCount; i++) {
                    String fieldClassName = rsmd.getColumnClassName(i);

                    String fieldName = rsmd.getColumnLabel(i);
                    JDBCUtil._recordMappingToMap(fieldClassName, fieldName, rs, valueMap);
                    set.add(fieldName);
                }
                records.add(valueMap);

                map.put("records", records);
                map.put("cloums", set);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("查询数据失败");
        }

        return map;
    }

    /**
     * 将ResultSet结果集中的记录映射到Map对象中.
     *
     * @param fieldClassName 是JDBC API中的类型名称,
     * @param fieldName      是字段名，
     * @param rs             是一个ResultSet查询结果集,
     * @param fieldValue     Map对象,用于存贮一条记录.
     * @throws SQLException
     */
    private static void _recordMappingToMap(String fieldClassName, String fieldName, ResultSet rs, Map fieldValue)
            throws SQLException {
        fieldName = fieldName.toLowerCase();
        // 优先规则：常用类型靠前
        if (fieldClassName.equals("java.lang.String")) {
            String s = rs.getString(fieldName);
            if (rs.wasNull()) {
                fieldValue.put("\"" + fieldName + "\"", null);
            } else {
                fieldValue.put("\"" + fieldName + "\"", "\"" + s + "\"");
            }
        } else if (fieldClassName.equals("java.lang.Integer")) {
            int s = rs.getInt(fieldName);
            if (rs.wasNull()) {
                fieldValue.put("\"" + fieldName + "\"", null);
            } else {
                fieldValue.put("\"" + fieldName + "\"", "\"" + s + "\"");// 早期jdk需要包装，jdk1.5后不需要包装
            }
        } else if (fieldClassName.equals("java.lang.Long")) {
            long s = rs.getLong(fieldName);
            if (rs.wasNull()) {
                fieldValue.put("\"" + fieldName + "\"", null);
            } else {
                fieldValue.put("\"" + fieldName + "\"", "\"" + s + "\"");
            }
        } else if (fieldClassName.equals("java.lang.Boolean")) {
            boolean s = rs.getBoolean(fieldName);
            if (rs.wasNull()) {
                fieldValue.put("\"" + fieldName + "\"", null);
            } else {
                fieldValue.put("\"" + fieldName + "\"", "\"" + s + "\"");
            }
        } else if (fieldClassName.equals("java.lang.Short")) {
            short s = rs.getShort(fieldName);
            if (rs.wasNull()) {
                fieldValue.put("\"" + fieldName + "\"", null);
            } else {
                fieldValue.put("\"" + fieldName + "\"", "\"" + s + "\"");
            }
        } else if (fieldClassName.equals("java.lang.Float")) {
            float s = rs.getFloat(fieldName);
            if (rs.wasNull()) {
                fieldValue.put("\"" + fieldName + "\"", null);
            } else {
                fieldValue.put("\"" + fieldName + "\"", "\"" + s + "\"");
            }
        } else if (fieldClassName.equals("java.lang.Double")) {
            double s = rs.getDouble(fieldName);
            if (rs.wasNull()) {
                fieldValue.put("\"" + fieldName + "\"", null);
            } else {
                fieldValue.put("\"" + fieldName + "\"", "\"" + s + "\"");
            }
        } else if (fieldClassName.equals("java.sql.Timestamp")) {
            Timestamp s = rs.getTimestamp(fieldName);
            if (rs.wasNull()) {
                fieldValue.put("\"" + fieldName + "\"", null);
            } else {
                fieldValue.put("\"" + fieldName + "\"", "\"" + s + "\"");
            }
        } else if (fieldClassName.equals("java.sql.Date") || fieldClassName.equals("java.util.Date")) {
            java.util.Date s = rs.getDate(fieldName);
            if (rs.wasNull()) {
                fieldValue.put("\"" + fieldName + "\"", null);
            } else {
                fieldValue.put("\"" + fieldName + "\"", "\"" + s + "\"");
            }
        } else if (fieldClassName.equals("java.sql.Time")) {
            Time s = rs.getTime(fieldName);
            if (rs.wasNull()) {
                fieldValue.put("\"" + fieldName + "\"", null);
            } else {
                fieldValue.put("\"" + fieldName + "\"", "\"" + s + "\"");
            }
        } else if (fieldClassName.equals("java.lang.Byte")) {
            byte s = rs.getByte(fieldName);
            if (rs.wasNull()) {
                fieldValue.put("\"" + fieldName + "\"", null);
            } else {
                fieldValue.put("\"" + fieldName + "\"", new Byte(s));
            }
        } else if (fieldClassName.equals("[B") || fieldClassName.equals("byte[]")) {
            // byte[]出现在SQL Server中
            byte[] s = rs.getBytes(fieldName);
            if (rs.wasNull()) {
                fieldValue.put("\"" + fieldName + "\"", null);
            } else {
                fieldValue.put("\"" + fieldName + "\"", "\"" + s + "\"");
            }
        } else if (fieldClassName.equals("java.math.BigDecimal")) {
            BigDecimal s = rs.getBigDecimal(fieldName);
            if (rs.wasNull()) {
                fieldValue.put("\"" + fieldName + "\"", null);
            } else {
                fieldValue.put("\"" + fieldName + "\"", "\"" + s + "\"");
            }
        } else if (fieldClassName.equals("java.lang.Object") || fieldClassName.equals("oracle.sql.STRUCT")) {
            Object s = rs.getObject(fieldName);
            if (rs.wasNull()) {
                fieldValue.put("\"" + fieldName + "\"", null);
            } else {
                fieldValue.put("\"" + fieldName + "\"", "\"" + s + "\"");
            }
        } else if (fieldClassName.equals("java.sql.Array") || fieldClassName.equals("oracle.sql.ARRAY")) {
            Array s = rs.getArray(fieldName);
            if (rs.wasNull()) {
                fieldValue.put("\"" + fieldName + "\"", null);
            } else {
                fieldValue.put("\"" + fieldName + "\"", "\"" + s + "\"");
            }
        } else if (fieldClassName.equals("java.sql.Clob")) {
            Clob s = rs.getClob(fieldName);
            if (rs.wasNull()) {
                fieldValue.put("\"" + fieldName + "\"", null);
            } else {
                fieldValue.put("\"" + fieldName + "\"", "\"" + s + "\"");
            }
        } else if (fieldClassName.equals("java.sql.Blob")) {
            Blob s = rs.getBlob(fieldName);
            if (rs.wasNull()) {
                fieldValue.put("\"" + fieldName + "\"", null);
            } else {
                fieldValue.put("\"" + fieldName + "\"", "\"" + s + "\"");
            }
        } else {// 对于其它任何未知类型的处理
            Object s = rs.getObject(fieldName);
            if (rs.wasNull()) {
                fieldValue.put("\"" + fieldName + "\"", null);
            } else {
                fieldValue.put("\"" + fieldName + "\"", "\"" + s + "\"");
            }
        }
    }

}
