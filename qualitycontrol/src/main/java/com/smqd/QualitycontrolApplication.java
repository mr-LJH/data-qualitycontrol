package com.smqd;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.smqd.dao")
public class QualitycontrolApplication {

    public static void main(String[] args) {
        SpringApplication.run(QualitycontrolApplication.class, args);
    }

}
