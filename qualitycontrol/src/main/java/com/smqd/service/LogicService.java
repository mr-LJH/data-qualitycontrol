package com.smqd.service;

import com.smqd.dao.LogicMapper;
import com.smqd.entity.DataBaseMessage;
import com.smqd.entity.QualityResult;
import com.smqd.entity.QuanlityTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author it
 */
@Service
public class LogicService {
    @Autowired(required = false)
    LogicMapper logicMapper;

    /**
     * 查询医院库中所有数据
     */
    public List<DataBaseMessage> selectAll() {
        return logicMapper.selectAll();
    }

    /***
     *  将查询出来的数据和插入到表中
     */
    public void insertQualityResult(QualityResult qualityResult) {

        logicMapper.insertQualityResult(qualityResult);
    }

    /***
     * /查询质控结果表
     * @return
     */
    public List<QualityResult> findQuanlitiResultAll() {
        return logicMapper.findQuanlitiResultAll();
    }

    /***
     * 查询质控表
     * @return
     */
    public List<QuanlityTable> selectQuanlityTable() {
        return logicMapper.selectQuanlityTable();
    }

    /***
     * 倒叙查询质控表
     * @param sourceUrl
     * @return
     */
    public QuanlityTable selectQuanlityTableDesc(String sourceUrl) {
        List<QuanlityTable> list = logicMapper.selectQuanlityTableDesc(sourceUrl);
        if (list.size() != 0) {
            return list.get(0);
        } else {
            return null;
        }

    }

    /***
     * 将查询出来的各个表中的数据插入到表中
     * @param quanlityTable
     */
    public void insertQuanlityTables(QuanlityTable quanlityTable) {
        logicMapper.insertQuanlityTables(quanlityTable);
    }

    /***
     * 更新表
     * @param table
     */
    public void updateQuanlityTable(QuanlityTable table) {
        logicMapper.updateQuanlityTable(table);
    }
}
