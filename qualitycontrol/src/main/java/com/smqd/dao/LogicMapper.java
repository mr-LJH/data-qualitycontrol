package com.smqd.dao;

import com.smqd.entity.DataBaseMessage;
import com.smqd.entity.QualityResult;
import com.smqd.entity.QuanlityTable;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface LogicMapper {

    /**
     * 查询医院库中所有数据
     */

    @Select("select * from data_base_message")
    List<DataBaseMessage> selectAll();

    /***
     *  将查询出来的数据和插入到表中
     */

    @Insert("insert into quality_result(sourceUrl,sourceTableName,sourceNum,targeNum,createTime,targeTableName) values(#{sourceUrl},#{sourceTableName},#{sourceNum},#{targeNum},#{createTime},#{targeTableName})")
    void insertQualityResult(QualityResult qualityResult);

    /***
     * /查询质控结果表
     * @return
     */
    @Select("select * from quality_result")
    List<QualityResult> findQuanlitiResultAll();

    /***
     * 查询质控表
     * @return
     */
    @Select("select * from quanlityTable")
    List<QuanlityTable> selectQuanlityTable();

    /***
     * 倒叙查询质控表
     * @param sourceUrl
     * @return
     */
    List<QuanlityTable> selectQuanlityTableDesc(@Param("sourceUrl") String sourceUrl);

    /***
     * 将查询出来的各个表中的数据插入到表中
     * @param quanlityTable
     */
    void insertQuanlityTables(QuanlityTable quanlityTable);

    /***
     * 更新表
     * @param table
     */
    @Update("update quanlityTable set sourceTableName = #{sourceTableName} , newTableName = #{newTableName} , createTime = #{createTime} where id = #{id} ")
    void updateQuanlityTable(QuanlityTable table);
}
